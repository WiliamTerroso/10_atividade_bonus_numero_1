#!/bin/bash
#
## Função para obter informações de clima
get_weather() {
  cidade=$1
    curl -s "wttr.in/$cidade?format=%C+%t"
    }
#
#    # Obter data e hora atual
data_hora_atual=$(date "+%d/%m/%Y %H:%M:%S")
#
#   # Obter informações de clima para João Pessoa e Recife
joao_pessoa_clima=$(get_weather "Joao Pessoa")
recife_clima=$(get_weather "Recife")
#
#  # Exibir a data e a hora atual
echo "Data e Hora Atual: $data_hora_atual"
#  #Exibir informações de clima para João Pessoa e Recife
echo "Clima em João Pessoa: $joao_pessoa_clima"
echo "Clima em Recife: $recife_clima"
#
